#include "GreaterThanOrEqualToUnsignedInteger64Validator"

#include <QtCore/QVariant>

namespace QtDataBinding {
namespace Validation {
GreaterThanOrEqualToUnsignedInteger64Validator::
GreaterThanOrEqualToUnsignedInteger64Validator(quint64  min,
                                               QObject* parent):
  Validator(parent),
  _min(min) {}

Validator::Status
GreaterThanOrEqualToUnsignedInteger64Validator::
validate(QVariant value) const {
  bool converted;
  auto number = value.toULongLong(&converted);

  if (!converted)
    return Status::failure;

  if (number >= _min)
    return Status::success;

  return Status::failure;
}

// MOC {{{
// ----------------------------------------------------------------------------
#ifdef MOC
  #include "moc_GreaterThanOrEqualToUnsignedInteger64Validator.moc.cpp"
#endif
// ----------------------------------------------------------------------------
// }}} MOC
} // namespace Validation
} // namespace QtDataBinding
