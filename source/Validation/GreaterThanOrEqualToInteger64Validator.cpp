#include "GreaterThanOrEqualToInteger64Validator"

#include <QtCore/QVariant>

namespace QtDataBinding {
namespace Validation {
GreaterThanOrEqualToInteger64Validator::
GreaterThanOrEqualToInteger64Validator(qint64   min,
                                       QObject* parent): Validator(parent),
                                                         _min(min) {}

Validator::Status
GreaterThanOrEqualToInteger64Validator::
validate(QVariant value) const {
  bool converted;
  auto number = value.toLongLong(&converted);

  if (!converted)
    return Status::failure;

  if (number >= _min)
    return Status::success;

  return Status::failure;
}

// MOC {{{
// ----------------------------------------------------------------------------
#ifdef MOC
  #include "moc_GreaterThanOrEqualToInteger64Validator.moc.cpp"
#endif
// ----------------------------------------------------------------------------
// }}} MOC
} // namespace Validation
} // namespace QtDataBinding
