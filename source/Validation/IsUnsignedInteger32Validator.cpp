#include "IsUnsignedInteger32Validator"

#include <QtCore/QVariant>

namespace QtDataBinding {
namespace Validation {
IsUnsignedInteger32Validator::
IsUnsignedInteger32Validator(QObject* parent): Validator(parent) {}

Validator::Status
IsUnsignedInteger32Validator::
validate(QVariant value) const {
  bool converted;

  value.toString().toUInt(&converted);

  if (!converted)
    return Status::failure;

  return Status::success;
}

// MOC {{{
// ----------------------------------------------------------------------------
#ifdef MOC
  #include "moc_IsUnsignedInteger32Validator.moc.cpp"
#endif
// ----------------------------------------------------------------------------
// }}} MOC
} // namespace Validation
} // namespace QtDataBinding
