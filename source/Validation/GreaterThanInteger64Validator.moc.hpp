#ifndef QtDataBinding_Validation_GreaterThanInteger64Validator_hpp
#define QtDataBinding_Validation_GreaterThanInteger64Validator_hpp

#include "Validator"

namespace QtDataBinding {
namespace Validation {
class GreaterThanInteger64Validator: public Validator {
  Q_OBJECT

public:
  GreaterThanInteger64Validator(qint64 min, QObject* parent = 0);

  Status
  validate(QVariant value) const;

private:
  qint64 _min;
};
} // namespace Validation
} // namespace QtDataBinding

#endif
