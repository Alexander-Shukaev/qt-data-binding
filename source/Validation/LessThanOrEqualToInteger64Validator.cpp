#include "LessThanOrEqualToInteger64Validator"

#include <QtCore/QVariant>

namespace QtDataBinding {
namespace Validation {
LessThanOrEqualToInteger64Validator::
LessThanOrEqualToInteger64Validator(qint64   max,
                                    QObject* parent): Validator(parent),
                                                      _max(max) {}

Validator::Status
LessThanOrEqualToInteger64Validator::
validate(QVariant value) const {
  bool converted;
  auto number = value.toLongLong(&converted);

  if (!converted)
    return Status::failure;

  if (number <= _max)
    return Status::success;

  return Status::failure;
}

// MOC {{{
// ----------------------------------------------------------------------------
#ifdef MOC
  #include "moc_LessThanOrEqualToInteger64Validator.moc.cpp"
#endif
// ----------------------------------------------------------------------------
// }}} MOC
} // namespace Validation
} // namespace QtDataBinding
