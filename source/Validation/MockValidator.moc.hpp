#ifndef QtDataBinding_Validation_MockValidator_hpp
#define QtDataBinding_Validation_MockValidator_hpp

#include "Validator"

namespace QtDataBinding {
namespace Validation {
class MockValidator: public Validator {
  Q_OBJECT

public:
  MockValidator(QObject* parent = 0);

  Status
  validate(QVariant value) const;
};
} // namespace Validation
} // namespace QtDataBinding

#endif
