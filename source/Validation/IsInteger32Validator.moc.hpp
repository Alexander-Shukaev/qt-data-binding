#ifndef QtDataBinding_Validation_IsInteger32Validator_hpp
#define QtDataBinding_Validation_IsInteger32Validator_hpp

#include "Validator"

namespace QtDataBinding {
namespace Validation {
class IsInteger32Validator: public Validator {
  Q_OBJECT

public:
  IsInteger32Validator(QObject* parent = 0);

  Status
  validate(QVariant value) const;
};
} // namespace Validation
} // namespace QtDataBinding

#endif
