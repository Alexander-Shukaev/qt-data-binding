#include "IsUnsignedInteger64Validator"

#include <QtCore/QVariant>

namespace QtDataBinding {
namespace Validation {
IsUnsignedInteger64Validator::
IsUnsignedInteger64Validator(QObject* parent): Validator(parent) {}

Validator::Status
IsUnsignedInteger64Validator::
validate(QVariant value) const {
  bool converted;

  value.toULongLong(&converted);

  if (!converted)
    return Status::failure;

  return Status::success;
}

// MOC {{{
// ----------------------------------------------------------------------------
#ifdef MOC
  #include "moc_IsUnsignedInteger64Validator.moc.cpp"
#endif
// ----------------------------------------------------------------------------
// }}} MOC
} // namespace Validation
} // namespace QtDataBinding
