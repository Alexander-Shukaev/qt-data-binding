#include "MockValidator"

#include <QtCore/QVariant>

namespace QtDataBinding {
namespace Validation {
MockValidator::
MockValidator(QObject* parent): Validator(parent) {}

Validator::Status
MockValidator::
validate(QVariant value) const
{ Q_UNUSED(value); return Status::success; }

// MOC {{{
// ----------------------------------------------------------------------------
#ifdef MOC
  #include "moc_MockValidator.moc.cpp"
#endif
// ----------------------------------------------------------------------------
// }}} MOC
} // namespace Validation
} // namespace QtDataBinding
