#include "LessThanUnsignedInteger64Validator"

#include <QtCore/QVariant>

namespace QtDataBinding {
namespace Validation {
LessThanUnsignedInteger64Validator::
LessThanUnsignedInteger64Validator(quint64  max,
                                   QObject* parent): Validator(parent),
                                                     _max(max) {}

Validator::Status
LessThanUnsignedInteger64Validator::
validate(QVariant value) const {
  bool converted;
  auto number = value.toULongLong(&converted);

  if (!converted)
    return Status::failure;

  if (number < _max)
    return Status::success;

  return Status::failure;
}

// MOC {{{
// ----------------------------------------------------------------------------
#ifdef MOC
  #include "moc_LessThanUnsignedInteger64Validator.moc.cpp"
#endif
// ----------------------------------------------------------------------------
// }}} MOC
} // namespace Validation
} // namespace QtDataBinding
