#include "LessThanOrEqualToReal64Validator"

#include <QtCore/QVariant>

namespace QtDataBinding {
namespace Validation {
LessThanOrEqualToReal64Validator::
LessThanOrEqualToReal64Validator(double   max,
                                 QObject* parent): Validator(parent),
                                                   _max(
                                                     max) {}

Validator::Status
LessThanOrEqualToReal64Validator::
validate(QVariant value) const {
  bool converted;
  auto number = value.toDouble(&converted);

  if (!converted)
    return Status::failure;

  if (number <= _max)
    return Status::success;

  return Status::failure;
}

// MOC {{{
// ----------------------------------------------------------------------------
#ifdef MOC
  #include "moc_LessThanOrEqualToReal64Validator.moc.cpp"
#endif
// ----------------------------------------------------------------------------
// }}} MOC
} // namespace Validation
} // namespace QtDataBinding
