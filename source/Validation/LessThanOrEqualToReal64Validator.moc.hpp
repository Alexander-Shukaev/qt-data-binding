#ifndef QtDataBinding_Validation_LessThanOrEqualToReal64Validator_hpp
#define QtDataBinding_Validation_LessThanOrEqualToReal64Validator_hpp

#include "Validator"

namespace QtDataBinding {
namespace Validation {
class LessThanOrEqualToReal64Validator: public Validator {
  Q_OBJECT

public:
  LessThanOrEqualToReal64Validator(double max, QObject* parent = 0);

  Status
  validate(QVariant value) const;

private:
  double _max;
};
} // namespace Validation
} // namespace QtDataBinding

#endif
