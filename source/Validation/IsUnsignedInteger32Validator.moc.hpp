#ifndef QtDataBinding_Validation_IsUnsignedInteger32Validator_hpp
#define QtDataBinding_Validation_IsUnsignedInteger32Validator_hpp

#include "Validator"

namespace QtDataBinding {
namespace Validation {
class IsUnsignedInteger32Validator: public Validator {
  Q_OBJECT

public:
  IsUnsignedInteger32Validator(QObject* parent = 0);

  Status
  validate(QVariant value) const;
};
} // namespace Validation
} // namespace QtDataBinding

#endif
