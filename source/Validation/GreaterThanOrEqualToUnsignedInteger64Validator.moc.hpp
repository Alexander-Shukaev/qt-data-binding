#ifndef \
  QtDataBinding_Validation_GreaterThanOrEqualToUnsignedInteger64Validator_hpp
#define \
  QtDataBinding_Validation_GreaterThanOrEqualToUnsignedInteger64Validator_hpp

#include "Validator"

namespace QtDataBinding {
namespace Validation {
class GreaterThanOrEqualToUnsignedInteger64Validator: public Validator {
  Q_OBJECT

public:
  GreaterThanOrEqualToUnsignedInteger64Validator(quint64  min,
                                                 QObject* parent = 0);

  Status
  validate(QVariant value) const;

private:
  quint64 _min;
};
} // namespace Validation
} // namespace QtDataBinding

#endif
