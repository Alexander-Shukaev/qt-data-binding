#include "IsInteger64Validator"

#include <QtCore/QVariant>

namespace QtDataBinding {
namespace Validation {
IsInteger64Validator::
IsInteger64Validator(QObject* parent): Validator(parent) {}

Validator::Status
IsInteger64Validator::
validate(QVariant value) const {
  bool converted;

  value.toLongLong(&converted);

  if (!converted)
    return Status::failure;

  return Status::success;
}

// MOC {{{
// ----------------------------------------------------------------------------
#ifdef MOC
  #include "moc_IsInteger64Validator.moc.cpp"
#endif
// ----------------------------------------------------------------------------
// }}} MOC
} // namespace Validation
} // namespace QtDataBinding
