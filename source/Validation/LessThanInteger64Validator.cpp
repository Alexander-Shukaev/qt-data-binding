#include "LessThanInteger64Validator"

#include <QtCore/QVariant>

namespace QtDataBinding {
namespace Validation {
LessThanInteger64Validator::
LessThanInteger64Validator(qint64 max, QObject* parent): Validator(parent),
                                                         _max(max) {}

Validator::Status
LessThanInteger64Validator::
validate(QVariant value) const {
  bool converted;
  auto number = value.toLongLong(&converted);

  if (!converted)
    return Status::failure;

  if (number < _max)
    return Status::success;

  return Status::failure;
}

// MOC {{{
// ----------------------------------------------------------------------------
#ifdef MOC
  #include "moc_LessThanInteger64Validator.moc.cpp"
#endif
// ----------------------------------------------------------------------------
// }}} MOC
} // namespace Validation
} // namespace QtDataBinding
