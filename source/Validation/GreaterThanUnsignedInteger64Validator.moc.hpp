#ifndef QtDataBinding_Validation_GreaterThanUnsignedInteger64Validator_hpp
#define QtDataBinding_Validation_GreaterThanUnsignedInteger64Validator_hpp

#include "Validator"

namespace QtDataBinding {
namespace Validation {
class GreaterThanUnsignedInteger64Validator: public Validator {
  Q_OBJECT

public:
  GreaterThanUnsignedInteger64Validator(quint64 min, QObject* parent = 0);

  Status
  validate(QVariant value) const;

private:
  quint64 _min;
};
} // namespace Validation
} // namespace QtDataBinding

#endif
