#ifndef QtDataBinding_Validation_IsUnsignedInteger64Validator_hpp
#define QtDataBinding_Validation_IsUnsignedInteger64Validator_hpp

#include "Validator"

namespace QtDataBinding {
namespace Validation {
class IsUnsignedInteger64Validator: public Validator {
  Q_OBJECT

public:
  IsUnsignedInteger64Validator(QObject* parent = 0);

  Status
  validate(QVariant value) const;
};
} // namespace Validation
} // namespace QtDataBinding

#endif
