#ifndef QtDataBinding_Validation_LessThanOrEqualToInteger64Validator_hpp
#define QtDataBinding_Validation_LessThanOrEqualToInteger64Validator_hpp

#include "Validator"

namespace QtDataBinding {
namespace Validation {
class LessThanOrEqualToInteger64Validator: public Validator {
  Q_OBJECT

public:
  LessThanOrEqualToInteger64Validator(qint64 max, QObject* parent = 0);

  Status
  validate(QVariant value) const;

private:
  qint64 _max;
};
} // namespace Validation
} // namespace QtDataBinding

#endif
