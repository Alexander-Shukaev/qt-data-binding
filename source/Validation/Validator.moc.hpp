#ifndef QtDataBinding_Validation_Validator_hpp
#define QtDataBinding_Validation_Validator_hpp

#include <QtCore/QObject>

class QVariant;

namespace QtDataBinding {
namespace Validation {
class Validator: public QObject {
  Q_OBJECT

public:
  enum class Status { failure, success };

  static Validator*
  greaterThanInteger64(qint64 min, QObject* parent = 0);

  static Validator*
  greaterThanUnsignedInteger64(quint64 min, QObject* parent = 0);

  static Validator*
  greaterThanReal64(double min, QObject* parent = 0);

  static Validator*
  greaterThanOrEqualToInteger64(qint64 min, QObject* parent = 0);

  static Validator*
  greaterThanOrEqualToUnsignedInteger64(quint64 min, QObject* parent = 0);

  static Validator*
  greaterThanOrEqualToReal64(double min, QObject* parent = 0);

  static Validator*
  lessThanInteger64(qint64 max, QObject* parent = 0);

  static Validator*
  lessThanUnsignedInteger64(quint64 max, QObject* parent = 0);

  static Validator*
  lessThanReal64(double max, QObject* parent = 0);

  static Validator*
  lessThanOrEqualToInteger64(qint64 max, QObject* parent = 0);

  static Validator*
  lessThanOrEqualToUnsignedInteger64(quint64 max, QObject* parent = 0);

  static Validator*
  lessThanOrEqualToReal64(double max, QObject* parent = 0);

  static Validator*
  isInteger32(QObject* parent = 0);

  static Validator*
  isUnsignedInteger32(QObject* parent = 0);

  static Validator*
  isInteger64(QObject* parent = 0);

  static Validator*
  isUnsignedInteger64(QObject* parent = 0);

  static Validator*
  regularExpression(QRegExp const& regularExpression, QObject* parent = 0);

  static Validator*
  mock(QObject* parent = 0);

  Validator(QObject* parent = 0);

  virtual
  ~Validator() = 0;

  virtual Status
  validate(QVariant value) const = 0;
};
} // namespace Validation
} // namespace QtDataBinding

#endif
