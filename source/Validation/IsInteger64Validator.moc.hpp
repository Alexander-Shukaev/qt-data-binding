#ifndef QtDataBinding_Validation_IsInteger64Validator_hpp
#define QtDataBinding_Validation_IsInteger64Validator_hpp

#include "Validator"

namespace QtDataBinding {
namespace Validation {
class IsInteger64Validator: public Validator {
  Q_OBJECT

public:
  IsInteger64Validator(QObject* parent = 0);

  Status
  validate(QVariant value) const;
};
} // namespace Validation
} // namespace QtDataBinding

#endif
