#ifndef \
  QtDataBinding_Validation_LessThanOrEqualToUnsignedInteger64Validator_hpp
#define \
  QtDataBinding_Validation_LessThanOrEqualToUnsignedInteger64Validator_hpp

#include "Validator"

namespace QtDataBinding {
namespace Validation {
class LessThanOrEqualToUnsignedInteger64Validator: public Validator {
  Q_OBJECT

public:
  LessThanOrEqualToUnsignedInteger64Validator(quint64  max,
                                              QObject* parent = 0);

  Status
  validate(QVariant value) const;

private:
  quint64 _max;
};
} // namespace Validation
} // namespace QtDataBinding

#endif
