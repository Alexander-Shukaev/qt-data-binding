#include "IsInteger32Validator"

#include <QtCore/QVariant>

namespace QtDataBinding {
namespace Validation {
IsInteger32Validator::
IsInteger32Validator(QObject* parent): Validator(parent) {}

Validator::Status
IsInteger32Validator::
validate(QVariant value) const {
  bool converted;

  value.toString().toInt(&converted);

  if (!converted)
    return Status::failure;

  return Status::success;
}

// MOC {{{
// ----------------------------------------------------------------------------
#ifdef MOC
  #include "moc_IsInteger32Validator.moc.cpp"
#endif
// ----------------------------------------------------------------------------
// }}} MOC
} // namespace Validation
} // namespace QtDataBinding
