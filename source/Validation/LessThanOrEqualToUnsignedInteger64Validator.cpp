#include "LessThanOrEqualToUnsignedInteger64Validator"

#include <QtCore/QVariant>

namespace QtDataBinding {
namespace Validation {
LessThanOrEqualToUnsignedInteger64Validator::
LessThanOrEqualToUnsignedInteger64Validator(quint64  max,
                                            QObject* parent): Validator(parent),
                                                              _max(max) {}

Validator::Status
LessThanOrEqualToUnsignedInteger64Validator::
validate(QVariant value) const {
  bool converted;
  auto number = value.toULongLong(&converted);

  if (!converted)
    return Status::failure;

  if (number <= _max)
    return Status::success;

  return Status::failure;
}

// MOC {{{
// ----------------------------------------------------------------------------
#ifdef MOC
  #include "moc_LessThanOrEqualToUnsignedInteger64Validator.moc.cpp"
#endif
// ----------------------------------------------------------------------------
// }}} MOC
} // namespace Validation
} // namespace QtDataBinding
