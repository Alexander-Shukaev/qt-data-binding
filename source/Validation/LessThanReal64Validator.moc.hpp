#ifndef QtDataBinding_Validation_LessThanReal64Validator_hpp
#define QtDataBinding_Validation_LessThanReal64Validator_hpp

#include "Validator"

namespace QtDataBinding {
namespace Validation {
class LessThanReal64Validator: public Validator {
  Q_OBJECT

public:
  LessThanReal64Validator(double max, QObject* parent = 0);

  Status
  validate(QVariant value) const;

private:
  double _max;
};
} // namespace Validation
} // namespace QtDataBinding

#endif
