#include "GreaterThanUnsignedInteger64Validator"

#include <QtCore/QVariant>

namespace QtDataBinding {
namespace Validation {
GreaterThanUnsignedInteger64Validator::
GreaterThanUnsignedInteger64Validator(quint64  min,
                                      QObject* parent): Validator(parent),
                                                        _min(min) {}

Validator::Status
GreaterThanUnsignedInteger64Validator::
validate(QVariant value) const {
  bool converted;
  auto number = value.toULongLong(&converted);

  if (!converted)
    return Status::failure;

  if (number > _min)
    return Status::success;

  return Status::failure;
}

// MOC {{{
// ----------------------------------------------------------------------------
#ifdef MOC
  #include "moc_GreaterThanUnsignedInteger64Validator.moc.cpp"
#endif
// ----------------------------------------------------------------------------
// }}} MOC
} // namespace Validation
} // namespace QtDataBinding
