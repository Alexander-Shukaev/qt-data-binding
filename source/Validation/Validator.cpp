#include "Validator"

#include "GreaterThanInteger64Validator"
#include "GreaterThanOrEqualToInteger64Validator"
#include "GreaterThanOrEqualToReal64Validator"
#include "GreaterThanOrEqualToUnsignedInteger64Validator"
#include "GreaterThanReal64Validator"
#include "GreaterThanUnsignedInteger64Validator"
#include "IsInteger32Validator"
#include "IsInteger64Validator"
#include "IsUnsignedInteger32Validator"
#include "IsUnsignedInteger64Validator"
#include "LessThanInteger64Validator"
#include "LessThanOrEqualToInteger64Validator"
#include "LessThanOrEqualToReal64Validator"
#include "LessThanOrEqualToUnsignedInteger64Validator"
#include "LessThanReal64Validator"
#include "LessThanUnsignedInteger64Validator"
#include "MockValidator"
#include "RegularExpressionValidator"

namespace QtDataBinding {
namespace Validation {
Validator*
Validator::
greaterThanInteger64(qint64 min, QObject* parent)
{ return new GreaterThanInteger64Validator(min, parent); }

Validator*
Validator::
greaterThanUnsignedInteger64(quint64 min, QObject* parent)
{ return new GreaterThanUnsignedInteger64Validator(min, parent); }

Validator*
Validator::
greaterThanReal64(double min, QObject* parent)
{ return new GreaterThanReal64Validator(min, parent); }

Validator*
Validator::
greaterThanOrEqualToInteger64(qint64 min, QObject* parent)
{ return new GreaterThanOrEqualToInteger64Validator(min, parent); }

Validator*
Validator::
greaterThanOrEqualToUnsignedInteger64(quint64 min, QObject* parent)
{ return new GreaterThanOrEqualToUnsignedInteger64Validator(min, parent); }

Validator*
Validator::
greaterThanOrEqualToReal64(double min, QObject* parent)
{ return new GreaterThanOrEqualToReal64Validator(min, parent); }

Validator*
Validator::
lessThanInteger64(qint64 max, QObject* parent)
{ return new LessThanInteger64Validator(max, parent); }

Validator*
Validator::
lessThanUnsignedInteger64(quint64 max, QObject* parent)
{ return new LessThanUnsignedInteger64Validator(max, parent); }

Validator*
Validator::
lessThanReal64(double max, QObject* parent)
{ return new LessThanReal64Validator(max, parent); }

Validator*
Validator::
lessThanOrEqualToInteger64(qint64 max, QObject* parent)
{ return new LessThanOrEqualToInteger64Validator(max, parent); }

Validator*
Validator::
lessThanOrEqualToUnsignedInteger64(quint64 max, QObject* parent)
{ return new LessThanOrEqualToUnsignedInteger64Validator(max, parent); }

Validator*
Validator::
lessThanOrEqualToReal64(double max, QObject* parent)
{ return new LessThanOrEqualToReal64Validator(max, parent); }

Validator*
Validator::
isInteger32(QObject* parent)
{ return new IsInteger32Validator(parent); }

Validator*
Validator::
isUnsignedInteger32(QObject* parent)
{ return new IsUnsignedInteger32Validator(parent); }

Validator*
Validator::
isInteger64(QObject* parent)
{ return new IsInteger64Validator(parent); }

Validator*
Validator::
isUnsignedInteger64(QObject* parent)
{ return new IsUnsignedInteger64Validator(parent); }

Validator*
Validator::
regularExpression(QRegExp const& regularExpression, QObject* parent)
{ return new RegularExpressionValidator(regularExpression, parent); }

Validator*
Validator::
mock(QObject* parent)
{ return new MockValidator(parent); }

Validator::
Validator(QObject* parent): QObject(parent) {}

Validator::
~Validator() {}

// MOC {{{
// ----------------------------------------------------------------------------
#ifdef MOC
  #include "moc_Validator.moc.cpp"
#endif
// ----------------------------------------------------------------------------
// }}} MOC
} // namespace Validation
} // namespace QtDataBinding
