#ifndef QtDataBinding_Validation_LessThanInteger64Validator_hpp
#define QtDataBinding_Validation_LessThanInteger64Validator_hpp

#include "Validator"

namespace QtDataBinding {
namespace Validation {
class LessThanInteger64Validator: public Validator {
  Q_OBJECT

public:
  LessThanInteger64Validator(qint64 max, QObject* parent = 0);

  Status
  validate(QVariant value) const;

private:
  qint64 _max;
};
} // namespace Validation
} // namespace QtDataBinding

#endif
