#include "GreaterThanOrEqualToReal64Validator"

#include <QtCore/QVariant>

namespace QtDataBinding {
namespace Validation {
GreaterThanOrEqualToReal64Validator::
GreaterThanOrEqualToReal64Validator(double   min,
                                    QObject* parent): Validator(parent),
                                                      _min(min) {}

Validator::Status
GreaterThanOrEqualToReal64Validator::
validate(QVariant value) const {
  bool converted;
  auto number = value.toDouble(&converted);

  if (!converted)
    return Status::failure;

  if (number >= _min)
    return Status::success;

  return Status::failure;
}

// MOC {{{
// ----------------------------------------------------------------------------
#ifdef MOC
  #include "moc_GreaterThanOrEqualToReal64Validator.moc.cpp"
#endif
// ----------------------------------------------------------------------------
// }}} MOC
} // namespace Validation
} // namespace QtDataBinding
