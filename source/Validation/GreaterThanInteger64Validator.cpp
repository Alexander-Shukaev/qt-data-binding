#include "GreaterThanInteger64Validator"

#include <QtCore/QVariant>

namespace QtDataBinding {
namespace Validation {
GreaterThanInteger64Validator::
GreaterThanInteger64Validator(qint64 min, QObject* parent): Validator(parent),
                                                            _min(min) {}

Validator::Status
GreaterThanInteger64Validator::
validate(QVariant value) const {
  bool converted;
  auto number = value.toLongLong(&converted);

  if (!converted)
    return Status::failure;

  if (number > _min)
    return Status::success;

  return Status::failure;
}

// MOC {{{
// ----------------------------------------------------------------------------
#ifdef MOC
  #include "moc_GreaterThanInteger64Validator.moc.cpp"
#endif
// ----------------------------------------------------------------------------
// }}} MOC
} // namespace Validation
} // namespace QtDataBinding
