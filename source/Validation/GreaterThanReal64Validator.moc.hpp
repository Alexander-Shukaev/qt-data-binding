#ifndef QtDataBinding_Validation_GreaterThanReal64Validator_hpp
#define QtDataBinding_Validation_GreaterThanReal64Validator_hpp

#include "Validator"

namespace QtDataBinding {
namespace Validation {
class GreaterThanReal64Validator: public Validator {
  Q_OBJECT

public:
  GreaterThanReal64Validator(double min, QObject* parent = 0);

  Status
  validate(QVariant value) const;

private:
  double _min;
};
} // namespace Validation
} // namespace QtDataBinding

#endif
