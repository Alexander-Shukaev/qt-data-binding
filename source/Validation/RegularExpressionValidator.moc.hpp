#ifndef QtDataBinding_Validation_RegularExpressionValidator_hpp
#define QtDataBinding_Validation_RegularExpressionValidator_hpp

#include "Validator"

#include "../Firewall/Implementation"

#include <QtCore/QScopedPointer>

class QRegExp;

namespace QtDataBinding {
namespace Validation {
class RegularExpressionValidatorPrivate;

class RegularExpressionValidator: public Validator {
  Q_OBJECT

public:
  RegularExpressionValidator(QRegExp const& regularExpression,
                             QObject*       parent = 0);

  ~RegularExpressionValidator();

  Status
  validate(QVariant value) const;

private:
  class Private;

  QScopedPointer<Private> implementation;

  QtDataBinding_Firewall_DECLARE_IMPLEMENTATION(Private)
};
} // namespace Validation
} // namespace QtDataBinding

#endif
