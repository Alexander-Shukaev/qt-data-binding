// Includes {{{
// ----------------------------------------------------------------------------
#include "RegularExpressionValidator"

// QtCore {{{
// ----------------------------------------------------------------------------
#include <QtCore/QVariant>
// ----------------------------------------------------------------------------
// }}} QtCore

// QtGui {{{
// ----------------------------------------------------------------------------
#include <QtGui/QRegExpValidator>
// ----------------------------------------------------------------------------
// }}} QtGui
// ----------------------------------------------------------------------------
// }}} Includes

namespace QtDataBinding {
namespace Validation {
class RegularExpressionValidator::Private {
public:
  QRegExpValidator* validator;

  Private(QRegExpValidator* validator);
}; // class RegularExpressionValidator::Private

// class RegularExpressionValidator {{{
// ----------------------------------------------------------------------------
RegularExpressionValidator::
RegularExpressionValidator(QRegExp const& regularExpression, QObject* parent):
  Validator(parent),
  implementation(new Private(new QRegExpValidator(regularExpression, this))) {}

RegularExpressionValidator::
~RegularExpressionValidator() {}

Validator::Status
RegularExpressionValidator::
validate(QVariant value) const {
  QtDataBinding_Firewall_USE_IMPLEMENTATION;

  auto string   = value.toString();
  int  position = 0;
  auto state    = im->validator->validate(string, position);

  if (state == QValidator::Acceptable)
    return Status::success;

  return Status::failure;
}
// ----------------------------------------------------------------------------
// }}} class RegularExpressionValidator

// class RegularExpressionValidator::Private {{{
// ----------------------------------------------------------------------------
RegularExpressionValidator::Private::
Private(QRegExpValidator* validator): validator(validator) {}
// ----------------------------------------------------------------------------
// }}} class RegularExpressionValidator::Private

// MOC {{{
// ----------------------------------------------------------------------------
#ifdef MOC
  #include "moc_RegularExpressionValidator.moc.cpp"
#endif
// ----------------------------------------------------------------------------
// }}} MOC
} // namespace Validation
} // namespace QtDataBinding
