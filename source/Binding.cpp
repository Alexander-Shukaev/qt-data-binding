// Includes {{{
// ----------------------------------------------------------------------------
#include "Binding"

#include "Context"
#include "Property"
#include "Signal"
#include "Slot"

#include "Conversion/Converter"
#include "Validation/Validator"

#include "Firewall/Interface"

// QtCore {{{
// ----------------------------------------------------------------------------
#include <QtCore/QList>
#include <QtCore/QPointer>
#include <QtCore/QVariant>
// ----------------------------------------------------------------------------
// }}} QtCore

// Standard Library {{{
// ----------------------------------------------------------------------------
#include <utility>
// ----------------------------------------------------------------------------
// }}} Standard Library
// ----------------------------------------------------------------------------
// }}} Includes

namespace QtDataBinding {
// Using Declarations {{{
// ----------------------------------------------------------------------------
namespace {
using Conversion::Converter;
using Validation::Validator;

using std::move;
}
// ----------------------------------------------------------------------------
// }}} Using Declarations

// Forward Declarations {{{
// ----------------------------------------------------------------------------
namespace {}
// ----------------------------------------------------------------------------
// }}} Forward Declarations

namespace {
struct Stage {
  enum class Operation { conversion, validation };

  Operation operation;
  int       index;

  static Stage
  conversion(int index);

  static Stage
  validation(int index);

  Stage(Operation operation, int index);
}; // struct Stage
}

// Type Aliases {{{
// ----------------------------------------------------------------------------
namespace {
using Converters = QList<QPointer<Converter> >;
using Validators = QList<QPointer<Validator> >;

using Stages = QList<Stage>;
}
// ----------------------------------------------------------------------------
// }}} Type Aliases

class Binding::Private: public QObject {
  Q_OBJECT

public:
  QPointer<Property> from;
  QPointer<Property> to;

  Status status;

  bool started;

  Converters converters;
  Validators validators;

  Stages stages;

  QPointer<Context> context;

  Private(Property* from, Property* to, Binding* interface);

  void
  setStatus(Status status);

  bool
  validate(QVariant& value);

protected Q_SLOTS:
  bool
  // TODO: Can be connection problem.
  update(Property* property, QVariant value);

private:
  Binding* interface;

  QtDataBinding_Firewall_DECLARE_INTERFACE(Binding)
}; // class Binding::Private

// class Binding {{{
// ----------------------------------------------------------------------------
Binding::
Binding(Property* from,
        Property* to,
        QObject*  parent): QObject(parent),
                           implementation(new Private(from, to, this)) {}

Binding::
Binding(QObject*       fromOwner,
        QString const& fromName,
        QObject*       toOwner,
        QString const& toName,
        QObject*       parent): Binding(new Property(fromOwner,
                                                     fromName,
                                                     this),
                                        new Property(toOwner,
                                                     toName,
                                                     this),
                                        parent) {}

bool
Binding::
exists() const
{ return from() && to() && from()->exists() && to()->exists(); }

bool
Binding::
expired() const
{ return !exists(); }

bool
Binding::
started() const {
  QtDataBinding_Firewall_USE_IMPLEMENTATION;

  return exists() && from()->started() && im->started;
}

bool
Binding::
stopped() const
{ return !started(); }

bool
Binding::
updated() const
{ return exists() && from()->value() == to()->value(); }

Property*
Binding::
from() const
{ QtDataBinding_Firewall_USE_IMPLEMENTATION; return im->from; }

Property*
Binding::
to() const
{ QtDataBinding_Firewall_USE_IMPLEMENTATION; return im->to; }

Binding::Status
Binding::
status() const {
  QtDataBinding_Firewall_USE_IMPLEMENTATION;

  return exists() ? im->status : Status::unknown;
}

Signal
Binding::
notifySignal() const {
  return Signal(const_cast<Binding*>(this),
                "statusChanged(QtDataBinding::Binding*)");
}

Context*
Binding::
context() const
{ QtDataBinding_Firewall_USE_IMPLEMENTATION; return im->context; }

void
Binding::
clear() {
  QtDataBinding_Firewall_USE_IMPLEMENTATION;

  im->converters.clear();
  im->validators.clear();
  im->stages.clear();
}

bool
Binding::
start() {
  QtDataBinding_Firewall_USE_IMPLEMENTATION;

  if (started())
    return true;

  if (expired())
    return false;

  auto started = from()->start() && from()->addListener(im, "update");

  im->started |= started;

  return started;
}

bool
Binding::
stop() {
  QtDataBinding_Firewall_USE_IMPLEMENTATION;

  if (stopped())
    return true;

  if (expired())
    return true;

  auto stopped = from()->removeListener(im, "update");

  im->started &= !stopped;

  return stopped;
}

bool
Binding::
update() {
  QtDataBinding_Firewall_USE_IMPLEMENTATION;

  Q_ASSERT(exists());

  return im->update(from(), from()->value());
}

bool
Binding::
validate() {
  QtDataBinding_Firewall_USE_IMPLEMENTATION;

  Q_ASSERT(exists());

  auto value = from()->value();

  if (!im->validate(value)) {
    im->setStatus(Status::failure);

    return false;
  }

  if (to()->canConvert(move(value))) {
    im->setStatus(Status::success);

    return true;
  }

  im->setStatus(Status::failure);

  return false;
}

void
Binding::
addConverter(Converter* converter) {
  QtDataBinding_Firewall_USE_IMPLEMENTATION;

  int index = im->converters.size();

  im->stages.append(Stage::conversion(index));
  im->converters.append(converter);
}

void
Binding::
addValidator(Validator* validator) {
  QtDataBinding_Firewall_USE_IMPLEMENTATION;

  int index = im->validators.size();

  im->stages.append(Stage::validation(index));
  im->validators.append(validator);
}

bool
Binding::
addListener(QObject* slotOwner, QString const& slotName) {
  auto signal = notifySignal();
  auto slot   = signal.compatibleSlot(slotOwner, slotName);

  return signal.connect(slot);
}

bool
Binding::
removeListener(QObject* slotOwner, QString const& slotName) {
  auto signal = notifySignal();
  auto slot   = signal.compatibleSlot(slotOwner, slotName);

  return signal.disconnect(slot);
}

void
Binding::
setContext(Context* context)
{ QtDataBinding_Firewall_USE_IMPLEMENTATION; im->context = context; }
// ----------------------------------------------------------------------------
// }}} class Binding

// class Binding::Private {{{
// ----------------------------------------------------------------------------
Binding::Private::
Private(Property* from,
        Property* to,
        Binding*  interface): QObject(interface),
                              from(from),
                              to(to),
                              status(Status::unknown),
                              started(false),
                              interface(interface) {}

void
Binding::Private::
setStatus(Status status_) {
  QtDataBinding_Firewall_USE_INTERFACE;

  if (status_ == status)
    return;

  status = status_;

  emit in->statusChanged(in);
}

bool
Binding::Private::
validate(QVariant& value) {
  for (auto stage : stages) {
    switch (stage.operation) {
      default :
      break;

    case Stage::Operation::validation: {
      auto validator = validators[stage.index];

      if (validator->validate(value) == Validator::Status::failure)
        return false;

      break;
    }

    case Stage::Operation::conversion: {
      auto converter = converters[stage.index];

      value = converter->convert(value);

      break;
    }
    }
  }

  return true;
}

bool
Binding::Private::
update(Property* property, QVariant value) {
  QtDataBinding_Firewall_USE_INTERFACE;

  Q_UNUSED(property);
  Q_ASSERT(property == from);
  Q_ASSERT(value == from->value());

  // Prevents infinite update cycles and redundant updates.
  if (in->updated()) {
    setStatus(Status::success);

    return true;
  }

  // NOTE: Another way to prevent update cycles would be to restrict the number
  // of bindings on the same pairs of properties up to 2 simplex bindings,
  // which make up 1 duplex one. This can be done either through set in the
  // context or even more strictly through global set. Then, it would be easy
  // to check if complement binding exists and to disconnect it while the value
  // is being set. However, benefits of such approach are questionable since
  // the imposed restrictions drop flexibility.

  if (!validate(value)) {
    setStatus(Status::failure);

    return false;
  }

  // NOTE: Never think of using `Property::setValue()` with `silently = true`
  // here. The reason is simple: there are might be some other objects in the
  // application that are listening to the changes of `to` property (most
  // probably other `Binding` instances).
  if (to->setValue(value)) {
    setStatus(Status::success);

    return true;
  }

  setStatus(Status::failure);

  return false;
}
// ----------------------------------------------------------------------------
// }}} class Binding::Private

// class Stage {{{
// ----------------------------------------------------------------------------
namespace {
Stage::
Stage(Operation operation, int index): operation(operation), index(index) {}

Stage
Stage::
conversion(int index)
{ return Stage(Operation::conversion, index); }

Stage
Stage::
validation(int index)
{ return Stage(Operation::validation, index); }
}
// ----------------------------------------------------------------------------
// }}} class Stage

// Definitions {{{
// ----------------------------------------------------------------------------
namespace {}
// ----------------------------------------------------------------------------
// }}} Definitions

// MOC {{{
// ----------------------------------------------------------------------------
#include "Binding.moc"

#ifdef MOC
  #include "moc_Binding.moc.cpp"
#endif
// ----------------------------------------------------------------------------
// }}} MOC
} // namespace QtDataBinding
