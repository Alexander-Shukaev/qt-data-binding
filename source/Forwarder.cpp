// Includes {{{
// ----------------------------------------------------------------------------
#include "Forwarder"
// ----------------------------------------------------------------------------
// }}} Includes

namespace QtDataBinding {
// class Forwarder {{{
// ----------------------------------------------------------------------------
Forwarder::
Forwarder(QObject* parent): QObject(parent) {}

Forwarder::
~Forwarder() {}

QString
Forwarder::
forwardSlotName() const
{ return "forward"; }
// ----------------------------------------------------------------------------
// }}} class Forwarder

// MOC {{{
// ----------------------------------------------------------------------------
#ifdef MOC
  #include "moc_Forwarder.moc.cpp"
#endif
// ----------------------------------------------------------------------------
// }}} MOC
} // namespace QtDataBinding
