#ifndef QtDataBinding_Signal_hpp
#define QtDataBinding_Signal_hpp

#include "Method"

namespace QtDataBinding {
class Slot;

class Signal: public Method {
public:
  Signal();

  Signal(QObject* owner, QMetaMethod const& metaMethod);

  Signal(QObject* owner, QString const& signature);

  bool
  exists() const;

  Slot
  compatibleSlot(QObject* owner) const;

  Slot
  compatibleSlot(QObject* owner, QString const& name) const;

  bool
  canConnect(Slot const& slot) const;

  bool
  connect(Slot const& slot) const;

  bool
  disconnect(Slot const& slot) const;
};
} // namespace QtDataBinding

#endif
