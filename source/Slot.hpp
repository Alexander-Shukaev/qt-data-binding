#ifndef QtDataBinding_Slot_hpp
#define QtDataBinding_Slot_hpp

#include "Method"

namespace QtDataBinding {
class Signal;

class Slot: public Method {
public:
  Slot();

  Slot(QObject* owner, QMetaMethod const& metaMethod);

  Slot(QObject* owner, QString const& signature);

  bool
  exists() const;

  Signal
  compatibleSignal(QObject* owner) const;

  Signal
  compatibleSignal(QObject* owner, QString const& name) const;

  bool
  canConnect(Signal const& signal) const;

  bool
  connect(Signal const& signal) const;

  bool
  disconnect(Signal const& signal) const;
};
} // namespace QtDataBinding

#endif
