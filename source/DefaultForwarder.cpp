// Includes {{{
// ----------------------------------------------------------------------------
#include "DefaultForwarder"

// QtCore {{{
// ----------------------------------------------------------------------------
#include <QtCore/QVariant>
// ----------------------------------------------------------------------------
// }}} QtCore
// ----------------------------------------------------------------------------
// }}} Includes

namespace QtDataBinding {
// class DefaultForwarder {{{
// ----------------------------------------------------------------------------
DefaultForwarder::
DefaultForwarder(QObject* parent): Forwarder(parent) {}

void
DefaultForwarder::
forward(int value) const
{ emit forwarded(value); }

void
DefaultForwarder::
forward(uint value) const
{ emit forwarded(value); }

void
DefaultForwarder::
forward(qlonglong value) const
{ emit forwarded(value); }

void
DefaultForwarder::
forward(qulonglong value) const
{ emit forwarded(value); }

void
DefaultForwarder::
forward(bool value) const
{ emit forwarded(value); }

void
DefaultForwarder::
forward(double value) const
{ emit forwarded(value); }

void
DefaultForwarder::
forward(float value) const
{ emit forwarded(value); }

void
DefaultForwarder::
forward(char const* value) const
{ emit forwarded(value); }

void
DefaultForwarder::
forward(QByteArray const& value) const
{ emit forwarded(value); }

void
DefaultForwarder::
forward(QBitArray const& value) const
{ emit forwarded(value); }

void
DefaultForwarder::
forward(QString const& value) const
{ emit forwarded(value); }
// ----------------------------------------------------------------------------
// }}} class DefaultForwarder

// MOC {{{
// ----------------------------------------------------------------------------
#ifdef MOC
  #include "moc_DefaultForwarder.moc.cpp"
#endif
// ----------------------------------------------------------------------------
// }}} MOC
} // namespace QtDataBinding
