// Includes {{{
// ----------------------------------------------------------------------------
#include "Context"

#include "Binding"
#include "Property"

#include "Firewall/Interface"

// QtCore {{{
// ----------------------------------------------------------------------------
#include <QtCore/QHash>
#include <QtCore/QPair>
// ----------------------------------------------------------------------------
// }}} QtCore
// ----------------------------------------------------------------------------
// }}} Includes

namespace QtDataBinding {
// Type Aliases {{{
// ----------------------------------------------------------------------------
namespace {
using PropertyKey = QPair<QObject*, QString>;

using BindingKey = QPair<PropertyKey, PropertyKey>;

using BindingHash  = QHash<BindingKey, Binding*>;
using PropertyHash = QHash<PropertyKey, Property*>;
}
// ----------------------------------------------------------------------------
// }}} Type Aliases

// Forward Declarations {{{
// ----------------------------------------------------------------------------
namespace {
BindingKey
bindingKey(Binding const* binding);

BindingKey
bindingKey(Property const* from, Property const* to);

PropertyKey
propertyKey(Property const* property);
}
// ----------------------------------------------------------------------------
// }}} Forward Declarations

class Context::Private: public QObject {
  Q_OBJECT

public:
  BindingHash  bindings;
  PropertyHash properties;

  Private(Context* interface);

  Binding*
  addBinding(Binding* binding);

  Property*
  addProperty(Property* property);

protected Q_SLOTS:
  void
  removeBinding(QObject* object);

  void
  removeProperty(QObject* object);

private:
  Context* interface;

  QtDataBinding_Firewall_DECLARE_INTERFACE(Context)
}; // Context::Private

// class Context {{{
// ----------------------------------------------------------------------------
Context::
Context(QObject* parent): QObject(parent), implementation(new Private(this)) {}

QList<Binding*>
Context::
bindings() const
{ QtDataBinding_Firewall_USE_IMPLEMENTATION; return im->bindings.values(); }

QList<Property*>
Context::
properties() const
{ QtDataBinding_Firewall_USE_IMPLEMENTATION; return im->properties.values(); }

Binding*
Context::
binding(Binding* binding) {
  QtDataBinding_Firewall_USE_IMPLEMENTATION;

  if (binding->context() == this)
    return binding;

  binding = new Binding(property(binding->from()),
                        property(binding->to()),
                        this);

  return im->addBinding(binding);
}

Binding*
Context::
binding(Property* from, Property* to) {
  QtDataBinding_Firewall_USE_IMPLEMENTATION;

  BindingKey bindingKey(propertyKey(from), propertyKey(to));

  auto binding = im->bindings[bindingKey];

  if (binding)
    return binding;

  binding = new Binding(property(from), property(to), this);

  return im->addBinding(binding);
}

Binding*
Context::
binding(QObject*       fromOwner,
        QString const& fromName,
        QObject*       toOwner,
        QString const& toName) {
  QtDataBinding_Firewall_USE_IMPLEMENTATION;

  BindingKey bindingKey(PropertyKey(fromOwner, fromName),
                        PropertyKey(toOwner, toName));

  auto binding = im->bindings[bindingKey];

  if (binding)
    return binding;

  binding = new Binding(property(fromOwner, fromName),
                        property(toOwner, toName),
                        this);

  return im->addBinding(binding);
}

Property*
Context::
property(Property* property) {
  QtDataBinding_Firewall_USE_IMPLEMENTATION;

  if (property->context() == this)
    return property;

  property = new Property(property, this);

  return im->addProperty(property);
}

Property*
Context::
property(QObject* owner, QString const& name) {
  QtDataBinding_Firewall_USE_IMPLEMENTATION;

  PropertyKey propertyKey(owner, name);

  auto property = im->properties[propertyKey];

  if (property)
    return property;

  property = new Property(owner, name, this);

  return im->addProperty(property);
}
// ----------------------------------------------------------------------------
// }}} class Context

// class Context::Private {{{
// ----------------------------------------------------------------------------
Context::Private::
Private(Context* interface): QObject(interface), interface(interface) {}

Binding*
Context::Private::
addBinding(Binding* binding) {
  QtDataBinding_Firewall_USE_INTERFACE;

  binding->setContext(in);

  connect(binding,
          SIGNAL(destroyed(QObject*)),
          this,
          SLOT(removeBinding(QObject*)));

  return bindings[bindingKey(binding)] = binding;
}

Property*
Context::Private::
addProperty(Property* property) {
  QtDataBinding_Firewall_USE_INTERFACE;

  property->setContext(in);

  connect(property,
          SIGNAL(destroyed(QObject*)),
          this,
          SLOT(removeProperty(QObject*)));

  return properties[propertyKey(property)] = property;
}

void
Context::Private::
removeBinding(QObject* object) {
  auto binding = qobject_cast<Binding*>(object);
  auto removed = bindings.remove(bindingKey(binding));

  Q_UNUSED(removed);
  Q_ASSERT(removed);
}

void
Context::Private::
removeProperty(QObject* object) {
  auto property = qobject_cast<Property*>(object);
  auto removed  = properties.remove(propertyKey(property));

  Q_UNUSED(removed);
  Q_ASSERT(removed);
}
// ----------------------------------------------------------------------------
// }}} class Context::Private

// Definitions {{{
// ----------------------------------------------------------------------------
namespace {
BindingKey
bindingKey(Binding const* binding)
{ return bindingKey(binding->from(), binding->to()); }

BindingKey
bindingKey(Property const* from, Property const* to)
{ return BindingKey(propertyKey(from), propertyKey(to)); }

PropertyKey
propertyKey(Property const* property)
{ return PropertyKey(property->owner(), property->name()); }
}
// ----------------------------------------------------------------------------
// }}} Definitions

// MOC {{{
// ----------------------------------------------------------------------------
#include "Context.moc"

#ifdef MOC
  #include "moc_Context.moc.cpp"
#endif
// ----------------------------------------------------------------------------
// }}} MOC
} // namespace QtDataBinding
