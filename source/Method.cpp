// Includes {{{
// ----------------------------------------------------------------------------
#include "Method"

#include "Signal"
#include "Slot"

// QtCore {{{
// ----------------------------------------------------------------------------
#include <QtCore/QMetaMethod>
#include <QtCore/QObject>
#include <QtCore/QPointer>
// ----------------------------------------------------------------------------
// }}} QtCore

// Standard Library {{{
// ----------------------------------------------------------------------------
#include <utility>
// ----------------------------------------------------------------------------
// }}} Standard Library
// ----------------------------------------------------------------------------
// }}} Includes

namespace QtDataBinding {
// Using Declarations {{{
// ----------------------------------------------------------------------------
namespace {
using std::move;
}
// ----------------------------------------------------------------------------
// }}} Using Declarations

class Method::Private {
public:
  QPointer<QObject> owner;
  QMetaMethod       metaMethod;

  Private(QObject* owner);

  Private(QObject* owner, QMetaMethod const& metaMethod);
}; // class Method::Private

// class Method {{{
// ----------------------------------------------------------------------------
Method::
Method(): Method(0) {}

Method::
Method(QObject* owner): implementation(new Private(owner)) {}

Method::
Method(QObject* owner, QMetaMethod const& metaMethod):
  implementation(new Private(owner, metaMethod)) {}

Method::
Method(QObject*       owner,
       QString const& signature): Method(owner) {
  QtDataBinding_Firewall_USE_IMPLEMENTATION;

  auto metaObject = owner->metaObject();
  auto index      = metaObject->
                    indexOfMethod(QMetaObject::
                                  normalizedSignature(signature.
                                                      toAscii().
                                                      constData()));

  if (index >= 0)
    im->metaMethod = metaObject->method(index);
}

// NOTE: Do not remove this empty destructor. There is a flaw in the
// implementation of `QSharedPointer` in Qt 4 which requires one to define the
// destructor explicitly, even if it is empty.
Method::
~Method() {}

// Notice the extremely efficient implementation of copy constructor for this
// particular case, which is worth explanation. Since both `owner` and
// `metaMethod` are immutable from the client's point of view, they could be
// implicitly shared among multiple copies of `Method`. As a result, in order
// to avoid redundant copying, it is enough to merely copy `implementation`.
Method::
Method(Method const& other): implementation(other.implementation) {}

Method::
Method(Method&& other): implementation(move(other.implementation)) {}

// Notice the extremely efficient implementation of assignment operator for
// this particular case, which is worth explanation. Since both `owner` and
// `metaMethod` are immutable from the client's point of view, they could be
// implicitly shared among multiple copies of `Method`. As a result, in order
// to avoid redundant copying, it is enough to merely copy `implementation`.
Method&
Method::
operator=(Method const& other)
{ implementation = other.implementation; return *this; }

Method&
Method::
operator=(Method&& other)
{ implementation = move(other.implementation); return *this; }

bool
Method::
operator==(Method const& other) const
{ return owner() == other.owner() && signature() == other.signature(); }

bool
Method::
operator!=(Method const& other) const
{ return !(*this == other); }

bool
Method::
exists() const
{ return owner() && metaMethod().methodIndex() >= 0; }

bool
Method::
expired() const
{ return !exists(); }

bool
Method::
isSignal() const
{ return metaMethod().methodType() == QMetaMethod::Signal; }

bool
Method::
isSlot() const
{ return metaMethod().methodType() == QMetaMethod::Slot; }

QObject*
Method::
owner() const
{ QtDataBinding_Firewall_USE_IMPLEMENTATION; return im->owner; }

QMetaMethod const&
Method::
metaMethod() const
{ QtDataBinding_Firewall_USE_IMPLEMENTATION; return im->metaMethod; }

QString
Method::
signature() const
{ return metaMethod().signature(); }

Signal
Method::
toSignal() const
{ return Signal(owner(), metaMethod()); }

Slot
Method::
toSlot() const
{ return Slot(owner(), metaMethod()); }
// ----------------------------------------------------------------------------
// }}} class Method

// class Method::Private {{{
// ----------------------------------------------------------------------------
Method::Private::
Private(QObject* owner): owner(owner) {}

Method::Private::
Private(QObject*           owner,
        QMetaMethod const& metaMethod): owner(owner),
                                        metaMethod(metaMethod) {}
// ----------------------------------------------------------------------------
// }}} class Method::Private
} // namespace QtDataBinding

uint
qHash(QtDataBinding::Method const& method)
{ return qHash(method.signature()) ^ qHash(method.owner()); }
