#ifndef QtDataBinding_Forwarder_hpp
#define QtDataBinding_Forwarder_hpp

#include <QtCore/QObject>

class QVariant;

namespace QtDataBinding {
class Forwarder: public QObject {
  Q_OBJECT

public:
  Forwarder(QObject* parent = 0);

  virtual
  ~Forwarder() = 0;

  virtual QString
  forwardSlotName() const;

Q_SIGNALS:
  void
  forwarded(QVariant const& value) const;
};
} // namespace QtDataBinding

#endif
