// Includes {{{
// ----------------------------------------------------------------------------
#include "Property"

#include "Context"
#include "DefaultForwarder"
#include "Signal"
#include "Slot"

#include "Firewall/Interface"

// QtCore {{{
// ----------------------------------------------------------------------------
#include <QtCore/QMetaProperty>
#include <QtCore/QPointer>
#include <QtCore/QVariant>
// ----------------------------------------------------------------------------
// }}} QtCore
// ----------------------------------------------------------------------------
// }}} Includes

namespace QtDataBinding {
// Type Aliases {{{
// ----------------------------------------------------------------------------
namespace {}
// ----------------------------------------------------------------------------
// }}} Type Aliases

// Forward Declarations {{{
// ----------------------------------------------------------------------------
namespace {}
// ----------------------------------------------------------------------------
// }}} Forward Declarations

class Property::Private: public QObject {
  Q_OBJECT

public:
  QPointer<QObject> owner;
  QMetaProperty metaProperty;

  QPointer<Forwarder> forwarder;

  bool started;
  bool shielded;

  QVariant shield;
  QVariant snapshot;

  QPointer<Context> context;

  Private(QObject* owner, Property* interface);

  Private(QObject*             owner,
          QMetaProperty const& metaProperty,
          Property*            interface);

protected Q_SLOTS:
  void
  notify(QVariant const& value);

private:
  Property* interface;

  QtDataBinding_Firewall_DECLARE_INTERFACE(Property)
}; // class Property::Private

// class Property {{{
// ----------------------------------------------------------------------------
Property::
Property(QObject* owner,
         QObject* parent): QObject(parent),
                           implementation(new Private(owner, this)) {}

Property::
Property(QObject*             owner,
         QMetaProperty const& metaProperty,
         QObject*             parent): QObject(parent),
                                       implementation(new Private(owner,
                                                                  metaProperty,
                                                                  this)) {}

Property::
Property(QObject*       owner,
         QString const& name,
         QObject*       parent): Property(owner, parent) {
  QtDataBinding_Firewall_USE_IMPLEMENTATION;

  auto metaObject = owner->metaObject();
  auto index      = metaObject->indexOfProperty(name.toAscii().constData());

  if (index >= 0)
    im->metaProperty = metaObject->property(index);
}

Property::
Property(Property const* property,
         QObject*        parent): Property(property->owner(),
                                           property->metaProperty(),
                                           parent) {}

bool
Property::
exists() const
{ return owner() && metaProperty().propertyIndex() >= 0; }

bool
Property::
expired() const
{ return !exists(); }

bool
Property::
started() const
{ QtDataBinding_Firewall_USE_IMPLEMENTATION; return exists() && im->started; }

bool
Property::
stopped() const
{ return !started(); }

bool
Property::
shielded() const
{ QtDataBinding_Firewall_USE_IMPLEMENTATION; return im->shielded; }

QObject*
Property::
owner() const
{ QtDataBinding_Firewall_USE_IMPLEMENTATION; return im->owner; }

QMetaProperty const&
Property::
metaProperty() const
{ QtDataBinding_Firewall_USE_IMPLEMENTATION; return im->metaProperty; }

QString
Property::
name() const
{ return metaProperty().name(); }

Signal
Property::
notifySignal() const {
  return Signal(const_cast<Property*>(this),
                "valueChanged(Property*, QVariant const&)");
}

Context*
Property::
context() const
{ QtDataBinding_Firewall_USE_IMPLEMENTATION; return im->context; }

QVariant
Property::
value() const {
  QtDataBinding_Firewall_USE_IMPLEMENTATION;

  Q_ASSERT(exists());

  if (shielded())
    return im->shield;

  return metaProperty().read(owner());
}

bool
Property::
canConvert(QVariant value) const
{ return value.convert(metaProperty().type()); }

void
Property::
setShielded(bool shielded) {
  QtDataBinding_Firewall_USE_IMPLEMENTATION;

  if (im->shielded == shielded)
    return;

  if (shielded)
    im->shield = value();

  im->shielded = shielded;
}

bool
Property::
setValue(QVariant const& value, bool silently) {
  QtDataBinding_Firewall_USE_IMPLEMENTATION;

  Q_ASSERT(exists());

  auto started_ = started();

  if (shielded()) {
    auto converted = canConvert(value);

    if (!converted)
      return false;

    im->shield = value;

    if (started_ && !silently)
      emit valueChanged(this, im->shield);

    return true;
  }

  if (started_ && silently)
    stop();

  auto converted = metaProperty().write(owner(), value);

  if (started_ && silently)
    start();

  return converted;
}

void
Property::
impale() {
  QtDataBinding_Firewall_USE_IMPLEMENTATION;

  Q_ASSERT(exists());

  if (!shielded())
    return;

  auto converted = metaProperty().write(owner(), im->shield);

  Q_UNUSED(converted);
  Q_ASSERT(converted);
}

bool
Property::
start() {
  QtDataBinding_Firewall_USE_IMPLEMENTATION;

  if (started())
    return true;

  if (expired())
    return false;

  Signal signal(owner(), metaProperty().notifySignal());

  auto slot = signal.compatibleSlot(im->forwarder,
                                    im->forwarder->forwardSlotName());

  auto started = signal.connect(slot) &&
                 connect(im->forwarder,
                         SIGNAL(forwarded(QVariant const &)),
                         im,
                         SLOT(notify(QVariant const &)));

  im->started |= started;

  return started;
}

bool
Property::
stop() {
  QtDataBinding_Firewall_USE_IMPLEMENTATION;

  if (stopped())
    return true;

  if (expired())
    return true;

  Signal signal(owner(), metaProperty().notifySignal());

  auto slot = signal.compatibleSlot(im->forwarder,
                                    im->forwarder->forwardSlotName());

  auto stopped = signal.disconnect(slot) ||
                 disconnect(im->forwarder,
                            SIGNAL(forwarded(QVariant const &)),
                            im,
                            SLOT(notify(QVariant const &)));

  im->started &= !stopped;

  return stopped;
}

void
Property::
notify() {
  QtDataBinding_Firewall_USE_IMPLEMENTATION;

  Q_ASSERT(exists());

  if (shielded())
    emit valueChanged(this, im->shield);
  else
    emit valueChanged(this, value());
}

bool
Property::
addListener(QObject* slotOwner, QString const& slotName) {
  auto signal = notifySignal();
  auto slot   = signal.compatibleSlot(slotOwner, slotName);

  return signal.connect(slot);
}

bool
Property::
removeListener(QObject* slotOwner, QString const& slotName) {
  auto signal = notifySignal();
  auto slot   = signal.compatibleSlot(slotOwner, slotName);

  return signal.disconnect(slot);
}

void
Property::
setForwarder(Forwarder* forwarder) {
  QtDataBinding_Firewall_USE_IMPLEMENTATION;

  forwarder->setParent(im);

  if (im->forwarder)
    im->forwarder->deleteLater();

  im->forwarder = forwarder;
  im->started   = false;
}

void
Property::
setContext(Context* context)
{ QtDataBinding_Firewall_USE_IMPLEMENTATION; im->context = context; }
// ----------------------------------------------------------------------------
// }}} class Property

// class Property::Private {{{
// ----------------------------------------------------------------------------
Property::Private::
Private(QObject*  owner,
        Property* interface): QObject(interface),
                              owner(owner),
                              forwarder(new DefaultForwarder(this)),
                              started(false),
                              shielded(false),
                              interface(interface) {}

Property::Private::
Private(QObject*             owner,
        QMetaProperty const& metaProperty,
        Property*            interface): QObject(interface),
                                         owner(owner),
                                         metaProperty(metaProperty),
                                         forwarder(new DefaultForwarder(this)),
                                         started(false),
                                         shielded(false),
                                         interface(interface) {}

void
Property::Private::
notify(QVariant const& value) {
  QtDataBinding_Firewall_USE_INTERFACE;

  // When shielded, intercept propagation of value changes from real property.
  if (!shielded)
    emit in->valueChanged(in, value);
}
// ----------------------------------------------------------------------------
// }}} class Property::Private

// Definitions {{{
// ----------------------------------------------------------------------------
namespace {}
// ----------------------------------------------------------------------------
// }}} Definitions

// MOC {{{
// ----------------------------------------------------------------------------
#include "Property.moc"

#ifdef MOC
  #include "moc_Property.moc.cpp"
#endif
// ----------------------------------------------------------------------------
// }}} MOC
} // namespace QtDataBinding
