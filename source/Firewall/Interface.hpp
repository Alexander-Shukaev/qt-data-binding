#ifndef QtDataBinding_Firewall_Interface_hpp
#define QtDataBinding_Firewall_Interface_hpp

#define QtDataBinding_Firewall_DECLARE_INTERFACE_AS(Class, pointer) \
  using Interface_ = Class;                                         \
                                                                    \
  friend class Class;                                               \
                                                                    \
  inline Class*                                                     \
  interface_()                                                      \
  { return static_cast<Class*>(pointer); }                          \
                                                                    \
  inline Class const*                                               \
  interface_() const                                                \
  { return static_cast<Class const*>(pointer); }

#define QtDataBinding_Firewall_DECLARE_INTERFACE(Class) \
  QtDataBinding_Firewall_DECLARE_INTERFACE_AS(Class, interface)

#define QtDataBinding_Firewall_USE_INTERFACE_AS(pointer) \
  auto const pointer = interface_()

#define QtDataBinding_Firewall_USE_INTERFACE \
  QtDataBinding_Firewall_USE_INTERFACE_AS(in)

#endif
