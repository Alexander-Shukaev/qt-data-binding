#ifndef QtDataBinding_Firewall_Implementation_hpp
#define QtDataBinding_Firewall_Implementation_hpp

namespace QtDataBinding {
namespace Firewall {
template <typename T>
static inline T*
pointer_(T* pointer)
{ return pointer; }

template <typename SmartPointer>
static inline typename SmartPointer::pointer
pointer_(SmartPointer const& pointer)
{ return pointer.data(); }
}
}

#define QtDataBinding_Firewall_DECLARE_IMPLEMENTATION_AS(Class, pointer) \
  using Implementation_ = Class;                                         \
                                                                         \
  friend class Class;                                                    \
                                                                         \
  inline Class*                                                          \
  implementation_() {                                                    \
    return reinterpret_cast<Class*>                                      \
           (QtDataBinding::Firewall::pointer_(pointer));                 \
  }                                                                      \
                                                                         \
  inline Class const*                                                    \
  implementation_() const {                                              \
    return reinterpret_cast<Class const*>                                \
           (QtDataBinding::Firewall::pointer_(pointer));                 \
  }

#define QtDataBinding_Firewall_DECLARE_IMPLEMENTATION(Class) \
  QtDataBinding_Firewall_DECLARE_IMPLEMENTATION_AS(Class, implementation)

#define QtDataBinding_Firewall_USE_IMPLEMENTATION_AS(pointer) \
  auto const pointer = implementation_()

#define QtDataBinding_Firewall_USE_IMPLEMENTATION \
  QtDataBinding_Firewall_USE_IMPLEMENTATION_AS(im)

#endif
