#ifndef QtDataBinding_Context_hpp
#define QtDataBinding_Context_hpp

#include "Firewall/Implementation"

#include <QtCore/QObject>

template <class T>
class QList;

namespace QtDataBinding {
class Binding;
class Property;

class Context: public QObject {
  Q_OBJECT

public:
  Context(QObject* parent = 0);

  QList<Binding*>
  bindings() const;

  QList<Property*>
  properties() const;

  Binding*
  binding(Binding* binding);

  Binding*
  binding(Property* from, Property* to);

  Binding*
  binding(QObject*       fromOwner,
          QString const& fromName,
          QObject*       toOwner,
          QString const& toName);

  Property*
  property(Property* property);

  Property*
  property(QObject* owner, QString const& name);

private:
  class Private;

  Private* implementation;

  QtDataBinding_Firewall_DECLARE_IMPLEMENTATION(Private)
};
} // namespace QtDataBinding

#endif
