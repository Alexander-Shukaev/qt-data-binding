#ifndef QtDataBinding_Method_hpp
#define QtDataBinding_Method_hpp

#include "Firewall/Implementation"

#include <QtCore/QSharedPointer>

class QMetaMethod;
class QObject;
class QString;

namespace QtDataBinding {
class Signal;
class Slot;

class Method {
public:
  Method();

  Method(QObject* owner);

  Method(QObject* owner, QMetaMethod const& metaMethod);

  Method(QObject* owner, QString const& signature);

  ~Method();

  Method(Method const& other);

  Method(Method&& other);

  Method&
  operator=(Method const& other);

  Method&
  operator=(Method&& other);

  bool
  operator==(Method const& other) const;

  bool
  operator!=(Method const& other) const;

  virtual bool
  exists() const;

  bool
  expired() const;

  bool
  isSignal() const;

  bool
  isSlot() const;

  QObject*
  owner() const;

  QMetaMethod const&
  metaMethod() const;

  QString
  signature() const;

  Signal
  toSignal() const;

  Slot
  toSlot() const;

private:
  class Private;

  QSharedPointer<Private> implementation;

  QtDataBinding_Firewall_DECLARE_IMPLEMENTATION(Private)
};
} // namespace QtDataBinding

uint
qHash(QtDataBinding::Method const& method);

#endif
