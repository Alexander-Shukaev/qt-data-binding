// Includes {{{
// ----------------------------------------------------------------------------
#include "Signal"

#include "Slot"

// QtCore {{{
// ----------------------------------------------------------------------------
#include <QtCore/QMetaMethod>
// ----------------------------------------------------------------------------
// }}} QtCore
// ----------------------------------------------------------------------------
// }}} Includes

namespace QtDataBinding {
// class Signal {{{
// ----------------------------------------------------------------------------
Signal::
Signal(): Method() {}

Signal::
Signal(QObject* owner, QMetaMethod const& metaMethod): Method(owner,
                                                              metaMethod) {}

Signal::
Signal(QObject* owner, QString const& signature): Method(owner, signature) {}

bool
Signal::
exists() const
{ return Method::exists() && isSignal(); }

Slot
Signal::
compatibleSlot(QObject* owner) const {
  auto metaObject = owner->metaObject();

  for (int i = 0; i < metaObject->methodCount(); ++i) {
    Slot slot(owner, metaObject->method(i));

    if (slot.exists() && canConnect(slot))
      return slot;
  }

  return Slot();
}

Slot
Signal::
compatibleSlot(QObject* owner, QString const& name) const {
  auto metaObject = owner->metaObject();

  for (int i = 0; i < metaObject->methodCount(); ++i) {
    Slot slot(owner, metaObject->method(i));

    if (slot.exists() && canConnect(slot) &&
        slot.signature().startsWith(name))
      return slot;
  }

  return Slot();
}

bool
Signal::
canConnect(Slot const& slot) const {
  return QMetaObject::checkConnectArgs(metaMethod().signature(),
                                       slot.metaMethod().signature());
}

bool
Signal::
connect(Slot const& slot) const {
  return QObject::connect(owner(),
                          metaMethod(),
                          slot.owner(),
                          slot.metaMethod());
}

bool
Signal::
disconnect(Slot const& slot) const {
  return QObject::disconnect(owner(),
                             metaMethod(),
                             slot.owner(),
                             slot.metaMethod());
}
// ----------------------------------------------------------------------------
// }}} class Signal
} // namespace QtDataBinding
