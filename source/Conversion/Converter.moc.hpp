#ifndef QtDataBinding_Conversion_Converter_hpp
#define QtDataBinding_Conversion_Converter_hpp

#include <QtCore/QObject>

class QVariant;

namespace QtDataBinding {
namespace Conversion {
class Converter: public QObject {
  Q_OBJECT

public:
  static Converter*
  stringToInteger32(int base = 10, QObject* parent = 0);

  static Converter*
  stringToUnsignedInteger32(int base = 10, QObject* parent = 0);

  static Converter*
  stringToInteger64(int base = 10, QObject* parent = 0);

  static Converter*
  stringToUnsignedInteger64(int base = 10, QObject* parent = 0);

  static Converter*
  mock(QObject* parent = 0);

  Converter(QObject* parent = 0);

  virtual
  ~Converter() = 0;

  virtual QVariant
  convert(QVariant value) const = 0;
};
} // namespace Conversion
} // namespace QtDataBinding

#endif
