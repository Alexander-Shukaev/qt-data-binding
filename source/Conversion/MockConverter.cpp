#include "MockConverter"

#include <QtCore/QVariant>

namespace QtDataBinding {
namespace Conversion {
MockConverter::
MockConverter(QObject* parent): Converter(parent) {}

QVariant
MockConverter::
convert(QVariant value) const
{ return value; }

// MOC {{{
// ----------------------------------------------------------------------------
#ifdef MOC
  #include "moc_MockConverter.moc.cpp"
#endif
// ----------------------------------------------------------------------------
// }}} MOC
} // namespace Conversion
} // namespace QtDataBinding
