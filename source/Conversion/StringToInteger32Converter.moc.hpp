#ifndef QtDataBinding_Conversion_StringToInteger32Converter_hpp
#define QtDataBinding_Conversion_StringToInteger32Converter_hpp

#include "Converter"

namespace QtDataBinding {
namespace Conversion {
class StringToInteger32Converter: public Converter {
  Q_OBJECT

public:
  StringToInteger32Converter(int base = 10, QObject* parent = 0);

  QVariant
  convert(QVariant value) const;

private:
  int _base;
};
} // namespace Conversion
} // namespace QtDataBinding

#endif
