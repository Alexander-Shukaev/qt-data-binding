#ifndef QtDataBinding_Conversion_StringToUnsignedInteger64Converter_hpp
#define QtDataBinding_Conversion_StringToUnsignedInteger64Converter_hpp

#include "Converter"

namespace QtDataBinding {
namespace Conversion {
class StringToUnsignedInteger64Converter: public Converter {
  Q_OBJECT

public:
  StringToUnsignedInteger64Converter(int base = 10, QObject* parent = 0);

  QVariant
  convert(QVariant value) const;

private:
  int _base;
};
} // namespace Conversion
} // namespace QtDataBinding

#endif
