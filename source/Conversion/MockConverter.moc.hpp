#ifndef QtDataBinding_Conversion_MockConverter_hpp
#define QtDataBinding_Conversion_MockConverter_hpp

#include "Converter"

namespace QtDataBinding {
namespace Conversion {
class MockConverter: public Converter {
  Q_OBJECT

public:
  MockConverter(QObject* parent = 0);

  QVariant
  convert(QVariant value) const;
};
} // namespace Conversion
} // namespace QtDataBinding

#endif
