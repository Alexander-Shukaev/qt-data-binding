#include "StringToUnsignedInteger32Converter"

#include <QtCore/QVariant>

namespace QtDataBinding {
namespace Conversion {
StringToUnsignedInteger32Converter::
StringToUnsignedInteger32Converter(int      base,
                                   QObject* parent): Converter(parent),
                                                     _base(base) {}

QVariant
StringToUnsignedInteger32Converter::
convert(QVariant value) const {
  bool converted;
  auto number = value.toString().toUInt(&converted, _base);

  if (!converted)
    return QVariant();

  return QVariant(number);
}

// MOC {{{
// ----------------------------------------------------------------------------
#ifdef MOC
  #include "moc_StringToUnsignedInteger32Converter.moc.cpp"
#endif
// ----------------------------------------------------------------------------
// }}} MOC
} // namespace Conversion
} // namespace QtDataBinding
