#include "StringToInteger32Converter"

#include <QtCore/QVariant>

namespace QtDataBinding {
namespace Conversion {
StringToInteger32Converter::
StringToInteger32Converter(int base, QObject* parent): Converter(parent),
                                                       _base(base) {}

QVariant
StringToInteger32Converter::
convert(QVariant value) const {
  bool converted;
  auto number = value.toString().toInt(&converted, _base);

  if (!converted)
    return QVariant();

  return QVariant(number);
}

// MOC {{{
// ----------------------------------------------------------------------------
#ifdef MOC
  #include "moc_StringToInteger32Converter.moc.cpp"
#endif
// ----------------------------------------------------------------------------
// }}} MOC
} // namespace Conversion
} // namespace QtDataBinding
