#include "Converter"

#include "MockConverter"
#include "StringToInteger32Converter"
#include "StringToInteger64Converter"
#include "StringToUnsignedInteger32Converter"
#include "StringToUnsignedInteger64Converter"

namespace QtDataBinding {
namespace Conversion {
Converter*
stringToInteger32(int base = 10, QObject* parent = 0)
{ return new StringToInteger32Converter(base, parent); }

Converter*
stringToUnsignedInteger32(int base = 10, QObject* parent = 0)
{ return new StringToUnsignedInteger32Converter(base, parent); }

Converter*
stringToInteger64(int base = 10, QObject* parent = 0)
{ return new StringToInteger64Converter(base, parent); }

Converter*
stringToUnsignedInteger64(int base = 10, QObject* parent = 0)
{ return new StringToUnsignedInteger64Converter(base, parent); }

Converter*
mock(QObject* parent = 0)
{ return new MockConverter(parent); }

Converter::
Converter(QObject* parent): QObject(parent) {}

Converter::
~Converter() {}

// MOC {{{
// ----------------------------------------------------------------------------
#ifdef MOC
  #include "moc_Converter.moc.cpp"
#endif
// ----------------------------------------------------------------------------
// }}} MOC
} // namespace Conversion
} // namespace QtDataBinding
