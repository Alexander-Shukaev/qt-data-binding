#ifndef QtDataBinding_Conversion_StringToInteger64Converter_hpp
#define QtDataBinding_Conversion_StringToInteger64Converter_hpp

#include "Converter"

namespace QtDataBinding {
namespace Conversion {
class StringToInteger64Converter: public Converter {
  Q_OBJECT

public:
  StringToInteger64Converter(int base = 10, QObject* parent = 0);

  QVariant
  convert(QVariant value) const;

private:
  int _base;
};
} // namespace Conversion
} // namespace QtDataBinding

#endif
