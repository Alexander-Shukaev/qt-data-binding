#include "StringToInteger64Converter"

#include <QtCore/QVariant>

namespace QtDataBinding {
namespace Conversion {
StringToInteger64Converter::
StringToInteger64Converter(int base, QObject* parent): Converter(parent),
                                                       _base(base) {}

QVariant
StringToInteger64Converter::
convert(QVariant value) const {
  bool converted;
  auto number = value.toString().toLongLong(&converted, _base);

  if (!converted)
    return QVariant();

  return QVariant(number);
}

// MOC {{{
// ----------------------------------------------------------------------------
#ifdef MOC
  #include "moc_StringToInteger64Converter.moc.cpp"
#endif
// ----------------------------------------------------------------------------
// }}} MOC
} // namespace Conversion
} // namespace QtDataBinding
