#include "StringToUnsignedInteger64Converter"

#include <QtCore/QVariant>

namespace QtDataBinding {
namespace Conversion {
StringToUnsignedInteger64Converter::
StringToUnsignedInteger64Converter(int      base,
                                   QObject* parent): Converter(parent),
                                                     _base(base) {}

QVariant
StringToUnsignedInteger64Converter::
convert(QVariant value) const {
  bool converted;
  auto number = value.toString().toULongLong(&converted, _base);

  if (!converted)
    return QVariant();

  return QVariant(number);
}

// MOC {{{
// ----------------------------------------------------------------------------
#ifdef MOC
  #include "moc_StringToUnsignedInteger64Converter.moc.cpp"
#endif
// ----------------------------------------------------------------------------
// }}} MOC
} // namespace Conversion
} // namespace QtDataBinding
