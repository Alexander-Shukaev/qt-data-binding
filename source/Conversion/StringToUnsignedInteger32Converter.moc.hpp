#ifndef QtDataBinding_Conversion_StringToUnsignedInteger32Converter_hpp
#define QtDataBinding_Conversion_StringToUnsignedInteger32Converter_hpp

#include "Converter"

namespace QtDataBinding {
namespace Conversion {
class StringToUnsignedInteger32Converter: public Converter {
  Q_OBJECT

public:
  StringToUnsignedInteger32Converter(int base = 10, QObject* parent = 0);

  QVariant
  convert(QVariant value) const;

private:
  int _base;
};
} // namespace Conversion
} // namespace QtDataBinding

#endif
