// Includes {{{
// ----------------------------------------------------------------------------
#include "Slot"

#include "Signal"

// QtCore {{{
// ----------------------------------------------------------------------------
#include <QtCore/QMetaMethod>
// ----------------------------------------------------------------------------
// }}} QtCore
// ----------------------------------------------------------------------------
// }}} Includes

namespace QtDataBinding {
// class Slot {{{
// ----------------------------------------------------------------------------
Slot::
Slot(): Method() {}

Slot::
Slot(QObject* owner, QMetaMethod const& metaMethod): Method(owner,
                                                            metaMethod) {}

Slot::
Slot(QObject* owner, QString const& signature): Method(owner, signature) {}

bool
Slot::
exists() const
{ return Method::exists() && isSlot(); }

Signal
Slot::
compatibleSignal(QObject* owner) const {
  auto metaObject = owner->metaObject();

  for (int i = 0; i < metaObject->methodCount(); ++i) {
    Signal signal(owner, metaObject->method(i));

    if (signal.exists() && canConnect(signal))
      return signal;
  }

  return Signal();
}

Signal
Slot::
compatibleSignal(QObject* owner, QString const& name) const {
  auto metaObject = owner->metaObject();

  for (int i = 0; i < metaObject->methodCount(); ++i) {
    Signal signal(owner, metaObject->method(i));

    if (signal.exists() && canConnect(signal) &&
        signal.signature().startsWith(name))
      return signal;
  }

  return Signal();
}

bool
Slot::
canConnect(Signal const& signal) const {
  return QMetaObject::checkConnectArgs(signal.metaMethod().signature(),
                                       metaMethod().signature());
}

bool
Slot::
connect(Signal const& signal) const {
  return QObject::connect(signal.owner(),
                          signal.metaMethod(),
                          owner(),
                          metaMethod());
}

bool
Slot::
disconnect(Signal const& signal) const {
  return QObject::disconnect(signal.owner(),
                             signal.metaMethod(),
                             owner(),
                             metaMethod());
}
// ----------------------------------------------------------------------------
// }}} class Slot
} // namespace QtDataBinding
