#ifndef QtDataBinding_DefaultForwarder_hpp
#define QtDataBinding_DefaultForwarder_hpp

#include "Forwarder"

class QBitArray;

namespace QtDataBinding {
class DefaultForwarder: public Forwarder {
  Q_OBJECT

public:
  DefaultForwarder(QObject* parent = 0);

protected Q_SLOTS:
  void
  forward(int value) const;

  void
  forward(uint value) const;

  void
  forward(qlonglong value) const;

  void
  forward(qulonglong value) const;

  void
  forward(bool value) const;

  void
  forward(double value) const;

  void
  forward(float value) const;

  void
  forward(char const* value) const;

  void
  forward(QByteArray const& value) const;

  void
  forward(QBitArray const& value) const;

  void
  forward(QString const& value) const;
};
} // namespace QtDataBinding

#endif
