#ifndef QtDataBinding_Binding_hpp
#define QtDataBinding_Binding_hpp

#include "Firewall/Implementation"

#include <QtCore/QObject>

namespace QtDataBinding {
class Context;
class Property;
class Signal;

namespace Conversion {
class Converter;
}

namespace Validation {
class Validator;
}

class Binding: public QObject {
  Q_OBJECT

  friend class Context;

public:
  enum class Status { unknown, failure, success };

  Binding(Property* from, Property* to, QObject* parent = 0);

  Binding(QObject*       fromOwner,
          QString const& fromName,
          QObject*       toOwner,
          QString const& toName,
          QObject*       parent = 0);

  bool
  exists() const;

  bool
  expired() const;

  bool
  started() const;

  bool
  stopped() const;

  bool
  updated() const;

  Property*
  from() const;

  Property*
  to() const;

  Status
  status() const;

  Signal
  notifySignal() const;

  Context*
  context() const;

  void
  clear();

  bool
  start();

  bool
  stop();

  bool
  update();

  bool
  validate();

  void
  addConverter(Conversion::Converter* converter);

  void
  addValidator(Validation::Validator* validator);

  bool
  addListener(QObject* slotOwner, QString const& slotName);

  bool
  removeListener(QObject* slotOwner, QString const& slotName);

Q_SIGNALS:
  void
  statusChanged(QtDataBinding::Binding* binding) const;

private:
  void
  setContext(Context* context);

  class Private;

  Private* implementation;

  QtDataBinding_Firewall_DECLARE_IMPLEMENTATION(Private)
};
} // namespace QtDataBinding

#endif
