#ifndef QtDataBinding_Property_hpp
#define QtDataBinding_Property_hpp

#include "Firewall/Implementation"

#include <QtCore/QObject>

class QMetaProperty;
class QVariant;

namespace QtDataBinding {
class Context;
class Forwarder;
class Signal;

class Property: public QObject {
  Q_OBJECT

  friend class Context;

public:
  Property(QObject* owner, QObject* parent = 0);

  Property(QObject*             owner,
           QMetaProperty const& metaProperty,
           QObject*             parent = 0);

  Property(QObject* owner, QString const& name, QObject* parent = 0);

  Property(Property const* property, QObject* parent = 0);

  bool
  exists() const;

  bool
  expired() const;

  bool
  started() const;

  bool
  stopped() const;

  bool
  shielded() const;

  QObject*
  owner() const;

  QMetaProperty const&
  metaProperty() const;

  QString
  name() const;

  Signal
  notifySignal() const;

  Context*
  context() const;

  QVariant
  value() const;

  bool
  canConvert(QVariant value) const;

  void
  setShielded(bool shielded);

  bool
  setValue(QVariant const& value, bool silently = false);

  template <class Forwarder>
  void
  setForwarder()
  { setForwarder(new Forwarder); }

  void
  impale();

  bool
  start();

  bool
  stop();

  void
  notify();

  bool
  addListener(QObject* slotOwner, QString const& slotName);

  bool
  removeListener(QObject* slotOwner, QString const& slotName);

Q_SIGNALS:
  void
  valueChanged(Property* property, QVariant const& value) const;

private:
  void
  setForwarder(Forwarder* forwarder);

  void
  setContext(Context* context);

  class Private;

  Private* implementation;

  QtDataBinding_Firewall_DECLARE_IMPLEMENTATION(Private)
};
} // namespace QtDataBinding

#endif
